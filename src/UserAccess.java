import javax.swing.*;
import java.io.IOException;

/**
 * This is to allow users to access the word search feature.
 */
public class UserAccess
{
    public static void main(String[] args) throws IOException {
        if(args.length < 1)
        {
            System.out.println("Usage UserAccess.java filename_for_word_grid");
            return;
        }
        GridConstructor wordSearch = new GridConstructor(args[0]);
        WordFinder wordFinder = new WordFinder(wordSearch.getWordGrid());

        String input = "";
        while (true) {
            input = JOptionPane.showInputDialog("Search this (enter 0 to exit.");
            if(input.equals("0"))
            {
                break;
            }
            int occurrences = wordFinder.getCountOf(input);
            System.out.println(input + " is found " + occurrences + " times.");
        }
    }
}

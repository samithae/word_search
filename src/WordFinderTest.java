import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class WordFinderTest {
    @Test
    public void testGetGridElementsInDirection()
    {
        char[][] grid = {
                {'A','B','C','D'},
                {'E','F','G','H'},
                {'I','J','K','L'}
        };
        WordFinder wordFinder = new WordFinder(grid);
        ArrayList<String> elementsSouth = wordFinder.getGirdElementsInDirection(0,0,1,0);
        ArrayList<String> elementsSE = wordFinder.getGirdElementsInDirection(0,0,1,1);
        ArrayList<String> elementsNE = wordFinder.getGirdElementsInDirection(2,2,1,-1);

        Assert.assertEquals("ABCD", elementsSouth.get(0));
        Assert.assertEquals("DCBA", elementsSouth.get(1));

        Assert.assertEquals("AFK", elementsSE.get(0));
        Assert.assertEquals("KFA", elementsSE.get(1));

        Assert.assertEquals("KH", elementsNE.get(0));
        Assert.assertEquals("HK", elementsNE.get(1));
    }

    @Test
    public void testGetAllSearchStrings()
    {
        char[][] grid = {
                {'A','B','C','D'},
                {'E','F','G','H'},
                {'I','J','K','L'}
        };
        WordFinder wordFinder = new WordFinder(grid);
        List<String> elements = wordFinder.createSearchStrings();

        Assert.assertTrue(elements.contains("ABCD"));
        Assert.assertTrue(elements.contains("HGFE"));
        Assert.assertTrue(elements.contains("L"));
        Assert.assertTrue(elements.contains("IFC"));
        Assert.assertTrue(elements.contains("CFI"));
        Assert.assertTrue(elements.contains("DHL"));
        Assert.assertTrue(elements.contains("BE"));
    }

    @Test
    public void testSearchWordInString_NoOccurrences()
    {
        String findThis = "Oops";
        String toSearch = "IAmHiddEN";
        Assert.assertEquals(0, WordFinder.findOccurrencesOfThis(findThis, toSearch));
    }

    @Test
    public void testSearchWordInString_SingleOccurrence_Middle()
    {
        String findThis = "Hidden";
        String toSearch = "IAmHiddEN";
        Assert.assertEquals(1, WordFinder.findOccurrencesOfThis(findThis, toSearch));
    }

    @Test
    public void testSearchWordInString_SingleOccurrence_Start()
    {
        String findThis = "Hidden";
        String toSearch = "hiddENGEM";
        Assert.assertEquals(1, WordFinder.findOccurrencesOfThis(findThis, toSearch));
    }

    @Test
    public void testSearchWordInString_MultipleOccurrences()
    {
        String findThis = "THE";
        String toSearch = "INTHESEWORDSTHESTRING";
        Assert.assertEquals(2, WordFinder.findOccurrencesOfThis(findThis, toSearch));
    }

    @Test
    public void testGetCountOfWords()
    {
        char[][] grid = {
                {'g',	'r',	'a',	'm',	'p'},
                {'E',	't',	'Y',	'd',	'A'},
                {'t',	'e',	'p',	's',	'U'},
                {'u',	'a',	'p',	'D',	'L'},
                {'a',	'v',	'a',	'r',	'w'}
        };
        WordFinder wordFinder = new WordFinder(grid);

        Assert.assertEquals(1, wordFinder.getCountOf("paul"));
        Assert.assertEquals(1, wordFinder.getCountOf("marg"));
        Assert.assertEquals(4, wordFinder.getCountOf("pa"));
    }
}

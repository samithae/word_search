import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GridConstructorTest
{
    File file;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void setUp() {
        try {
            file = folder.newFile( "words.txt" );
        }
        catch(IOException ioe ) {
            System.err.println("error creating temporary test file in " +
                            this.getClass().getSimpleName() );
        }
    }

    @Test (expected = GridConstructor.GridFileNotFoundException.class)
    public void testLoadFile_GridFileNotFound() {
        String fileName = "words.txt";
        new GridConstructor(fileName);
    }

    @Test
    public void testLoadFile_ValidContents() throws IOException {
        String fileName = "words.txt";
        FileWriter fileWriter = new FileWriter( file );
        BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
        bufferedWriter.write( "A,B,C,D,E,F,G\n");
        bufferedWriter.write( "H,I,J,K,L,M,N\n");
        bufferedWriter.close();
        GridConstructor search = new GridConstructor(file.getAbsolutePath());
        char[][] grid = search.getWordGrid();
        for(int i=0;i<7;i++)
        {
            Assert.assertEquals((char) (65+i), grid[0][i]);
            Assert.assertEquals((char) (72+i), grid[1][i]);
        }
    }

    @Test
    public void testLoadFile_ValidContents_mixedCase() throws IOException {
        String fileName = "words.txt";
        FileWriter fileWriter = new FileWriter( file );
        BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
        bufferedWriter.write( "A,B,C,d,E,f,G\n");
        bufferedWriter.write( "H,I,j,K,l,m,N\n");
        bufferedWriter.close();
        GridConstructor search = new GridConstructor(file.getAbsolutePath());
        char[][] grid = search.getWordGrid();
        for(int i=0;i<7;i++)
        {
            Assert.assertEquals((char) (65+i), grid[0][i]);
            Assert.assertEquals((char) (72+i), grid[1][i]);
        }
    }

    @Test (expected = GridConstructor.GridColumnSizeNotConstantException.class)
    public void testLoadFile_ColumnSizeNotEqual() throws IOException {
        String fileName = "words.txt";
        FileWriter fileWriter = new FileWriter( file );
        BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
        bufferedWriter.write( "A,B,C,D,E,F,G\n");
        bufferedWriter.write( "H,I,J,K,L,M\n");
        bufferedWriter.close();
        GridConstructor search = new GridConstructor(file.getAbsolutePath());
        search.getWordGrid();
    }

    @Test (expected = GridConstructor.GridElementNotValidException.class)
    public void testLoadFile_TwoCharactersInCommaSeperatedElement() throws IOException {
        String fileName = "words.txt";
        FileWriter fileWriter = new FileWriter( file );
        BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
        bufferedWriter.write( "A,B,C,D,E,F,G\n");
        bufferedWriter.write( "H,I,J,K,L,M,NO\n");
        bufferedWriter.close();
        GridConstructor search = new GridConstructor(file.getAbsolutePath());
        search.getWordGrid();
    }

    @Test (expected = GridConstructor.GridElementNotValidException.class)
    public void testLoadFile_NoCharInCommaSeperatedElement() throws IOException {
        String fileName = "words.txt";
        FileWriter fileWriter = new FileWriter( file );
        BufferedWriter bufferedWriter = new BufferedWriter( fileWriter );
        bufferedWriter.write( "A,B,C,D,E,F,G\n");
        bufferedWriter.write( "H,I,J,K,,M,N\n");
        bufferedWriter.close();
        GridConstructor search = new GridConstructor(file.getAbsolutePath());
        search.getWordGrid();
    }
}

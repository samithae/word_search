import java.io.*;
import java.util.ArrayList;

/**
 * Constructs a character grid from a content of a comma seperated file
 */
public class GridConstructor
{
    private final File gridFile;

    /**
     * Constructor
     *
     * @param fileName Path to the file with grid elements
     */
    public GridConstructor(String fileName)
    {
        gridFile = new File(fileName);
        if(!gridFile.exists())
        {
            throw new GridFileNotFoundException(gridFile.getAbsolutePath());
        }
    }

    /**
     * @return the character grid from the file contents
     *
     * @throws IOException if a file read error occurred
     */
    public char[][] getWordGrid() throws IOException
    {
        ArrayList<String[]> lines = readCSVArrayLinesFromFile();

        int gridRows = lines.size();
        int gridCols = lines.get(0).length;

        char[][] grid = new char[gridRows][gridCols];

        for(int row = 0; row < gridRows; row++)
        {
            if(lines.get(row).length != gridCols)
            {
                throw new GridColumnSizeNotConstantException();
            }

            for(int col = 0; col < gridCols; col++)
            {
                String gridElement =  lines.get(row)[col].trim().toUpperCase();
                if(gridElement.length() == 0 || gridElement.length() > 1)
                {
                    throw new GridElementNotValidException();
                }
                grid[row][col] = gridElement.charAt(0);
            }
        }

        return grid;
    }

    /**
     * @return Comma separated elements as an array list
     *
     * @throws IOException if a file read error occurred
     */
    private ArrayList<String[]> readCSVArrayLinesFromFile() throws IOException
    {
        try (BufferedReader br = new BufferedReader(new FileReader(gridFile))) {
            ArrayList<String[]> charLines = new ArrayList();
            String line;
            while ((line = br.readLine()) != null)
            {
                // This is required to remove BOM character in certain files.
                // Java file reading does not automatically detect this.
                line = line.replace("\uFEFF", "");

                charLines.add(line.split(","));
            }
            return charLines;
        }
    }

    /**
     * Special exception when the given file is not available
     */
    public class GridFileNotFoundException extends RuntimeException
    {
        public GridFileNotFoundException(String absolutePath)
        {
            super("Word grid file not found : " + absolutePath);
        }
    }

    /**
     * Special exception when the Grid column sizes does not match
     */
    public class GridColumnSizeNotConstantException extends RuntimeException
    {
        public GridColumnSizeNotConstantException()
        {
            super("Word grid column size is not constant across the rows.");
        }
    }

    /**
     * Special exception when the comma separated element is not valid
     */
    public class GridElementNotValidException extends RuntimeException
    {
        public GridElementNotValidException()
        {
            super("Word grid element is not a valid character or it contains more than one character.");
        }
    }
}

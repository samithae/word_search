import java.util.ArrayList;
import java.util.List;

/**
 * Provides functionality to find a word in a given character grid.
 */
public class WordFinder {
    private final char[][] grid;

    /**
     * Constructor
     *
     * @param grid character grid
     */
    public WordFinder(char[][] grid)
    {
        this.grid = grid;
    }

    /**
     * @param findThis String to find
     *
     * @return number of times the given string appears in the character grid
     */
    public int getCountOf(String findThis)
    {
        List<String> searchStrings = createSearchStrings();
        int occurrences = 0;
        for(String toSearch : searchStrings)
        {
            occurrences += findOccurrencesOfThis(findThis, toSearch);
        }
        return  occurrences;
    }

    /**
     * @param findThis String to find
     * @param toSearch Search in this string
     *
     * @return number of times the given string appears in the search string.
     */
    static int findOccurrencesOfThis(String findThis, String toSearch)
    {
        int count=0;
        int index = toSearch.length();
        String searchString = toSearch;
        while(index > 0)
        {
            index = searchString.toUpperCase().lastIndexOf(findThis.toUpperCase());
            if (index >= 0)
            {
                count++;
                searchString = searchString.substring(0, index+findThis.length()-1).trim();
            }
        }
        return count;
    }

    /**
     * @return a list of all possible strings contained in the character grid
     */
    List<String> createSearchStrings()
    {
        int cols = grid[0].length;
        int rows = grid.length;

        ArrayList<String> searchStrings = new ArrayList();

        for(int col = 0; col < cols; col++)
        {
            searchStrings.addAll(getGirdElementsInDirection(col, 0, 0,1));
            searchStrings.addAll(getGirdElementsInDirection(col, 0, 1,1));
            searchStrings.addAll(getGirdElementsInDirection(col, 0, 1,1));
            searchStrings.addAll(getGirdElementsInDirection(col, rows - 1, 1,-1));
        }
        for(int row = 0; row < rows; row++)
        {
            searchStrings.addAll(getGirdElementsInDirection(0, row, 1,0));
            if(row > 0)
            {
                searchStrings.addAll(getGirdElementsInDirection(0, row, 1,1));
            }

            if(row < rows - 1)
            {
                searchStrings.addAll(getGirdElementsInDirection(0, row, 1,-1));
            }
        }
        return searchStrings;
    }

    /**
     * @param columnStart Start at this column
     * @param rowStart Start at this row
     * @param columnIncrement Increment columns by this
     * @param rowIncrement Increment rows by this
     *
     * @return Forward and reverse strings from a given start point in a given direction
     */
    ArrayList<String> getGirdElementsInDirection(int columnStart, int rowStart, int columnIncrement, int rowIncrement)
    {
        int numberOfColumns = grid[0].length;
        int numberOfRows = grid.length;
        ArrayList<String> stringElements = new ArrayList();
        StringBuilder stringBuilder = new StringBuilder();
        int row = rowStart;
        int col = columnStart;

        while(row >= 0 && row < numberOfRows && col >= 0 && col < numberOfColumns)
        {
            stringBuilder.append(grid[row][col]);
            row += rowIncrement;
            col += columnIncrement;
        }
        stringElements.add(stringBuilder.toString());
        if(stringBuilder.toString().length() > 1) {
            stringElements.add(stringBuilder.reverse().toString());
        }
        return stringElements;
    }


}
